package io.github.epocopa.criminalintent.database

import androidx.lifecycle.LiveData
import androidx.room.*
import io.github.epocopa.criminalintent.Crime
import java.util.*

@Dao
interface CrimeDAO {
    @Query("SELECT * FROM crime")
    fun getCrimes() : LiveData<List<Crime>>

    @Query("SELECT * FROM crime WHERE id=:id")
    fun getCrime(id: UUID) : LiveData<Crime?>

    @Update
    fun updateCrime(crime: Crime)

    @Delete
    fun deleteCrime(crime: Crime)

    @Insert
    fun addCrime(crime: Crime)
}