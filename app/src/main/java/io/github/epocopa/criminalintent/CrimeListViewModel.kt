package io.github.epocopa.criminalintent

import androidx.lifecycle.ViewModel

class CrimeListViewModel: ViewModel() {
    private val repository = CrimeRepository.get()
    val crimeListLiveData = repository.getCrimes()

    fun addCrime(crime: Crime) {
        repository.addCrime(crime)
    }
}