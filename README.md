App developed following best practices in Android development detailed in the book "Android Programming: The Big Nerd Ranch Guide"

Main language is Kotlin. Fragments are used, along with the Room Database, ViewModels and various android patterns. All from the Jetpack suite.


